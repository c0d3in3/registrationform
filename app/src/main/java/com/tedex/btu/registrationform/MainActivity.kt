package com.tedex.btu.registrationform

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import android.widget.Toast.LENGTH_SHORT
import kotlinx.android.synthetic.main.activity_main.*
import java.security.AccessController.getContext

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        registerButton.setOnClickListener(){
            if(emailEditText.text.toString().isEmpty() || nameEditText.text.toString().isEmpty() || passwordEditText.text.toString().isEmpty() || confirmPasswordEditText.text.toString().isEmpty()) {
                Toast.makeText(getApplicationContext(), "Please fill all fields", LENGTH_SHORT).show()
            }
            else{
                if(passwordEditText.text.toString().equals(confirmPasswordEditText.text.toString())){
                    Toast.makeText(getApplicationContext(), "Registration was successful", LENGTH_SHORT).show()
                }
            }
        }
    }
}
